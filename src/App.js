import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ListCar from "./pages/ListCar";
import Home from "./pages/Home";
import Detail from "./pages/Detail";
import Invoice from "./pages/Invoice";
import AddNewCar from "./pages/AddNewCar";
import Dashboard from "./pages/Dashboard";
import { HomeDashboard } from "./pages/HomeDashboard";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route element={<Dashboard />}>
          <Route path="dashboard" element={<HomeDashboard />}></Route>
          <Route path="dashboard/listCar" element={<ListCar />}></Route>
          <Route
            path="dashboard/listCar/addNewCar"
            element={<AddNewCar />}
          ></Route>
        </Route>
        <Route path="/detail/:id" element={<Detail />}></Route>
        <Route path="/invoice" element={<Invoice />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
