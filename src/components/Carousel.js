import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import photo from "../asset/img_photo.png";
import rate from "../asset/Rate.svg";
import "../style/Home.css";
import left from "../asset/left.png";
import right from "../asset/right.png";

const Carousel = () => {
  const leftIC = `<img src=${left} alt="left" />`;
  const rightIC = `<img src=${right} alt="right" />`;
  let owlCarouselVariable = {
    options: {
      stagePadding: 432,
      loop: true,
      nav: true,
      navText: [leftIC, rightIC],
      dots: false,
      responsive: {
        0: {
          items: 1,
          stagePadding: false,
        },

        576: {
          items: 1.2,
          stagePadding: 8,
        },

        768: {
          items: 1.2,
          stagePadding: 26,
        },

        992: {
          items: 1.4,
          stagePadding: 30,
        },

        1200: {
          items: 1,
          stagePadding: 300,
        },
        1440: {
          items: 1,
          stagePadding: 400,
        },
        1500: {
          items: 1,
          stagePadding: 430,
        },
      },
    },
  };

  return (
    <div>
      <div class="row title-testy justify-content-center w-100" id="testimony">
        <div class="col-lg-10 text-center">
          <h2>Testimonial</h2>
          <p>Berbagai review positif dari para pelanggan kami</p>
        </div>
      </div>
      <OwlCarousel
        class="owl-carousel owl-theme row"
        {...owlCarouselVariable.options}
      >
        <div class="item">
          <div class="item-testy d-block d-sm-flex align-items-center m-auto m-md-0">
            <div class="pic-testy me-xl-5">
              <img src={photo} class="m-auto m-sm-0" alt="pic" />
            </div>
            <div class="desc-testy">
              <img src={rate} class="mt-3 m-auto m-sm-0" alt="rate" />
              <p class="text-center text-sm-start">
                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod”
              </p>
              <p class=" nama-testy fw-bold text-center text-sm-start">
                John Dee 32, Bromo
              </p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="item-testy d-block d-sm-flex align-items-center m-auto m-md-0">
            <div class="pic-testy me-xl-5">
              <img src={photo} class="m-auto m-sm-0" alt="pic" />
            </div>
            <div class="desc-testy">
              <img src={rate} class="mt-3 m-auto m-sm-0" alt="rate" />
              <p class="text-center text-sm-start">
                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod”
              </p>
              <p class="fw-bold text-center text-sm-start">
                John Dee 32, Bromo
              </p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="item-testy d-block d-sm-flex align-items-center m-auto m-md-0">
            <div class="pic-testy me-xl-5">
              <img src={photo} class="m-auto m-sm-0" alt="pic" />
            </div>
            <div class="desc-testy">
              <img src={rate} class="mt-3 m-auto m-sm-0" alt="rate" />
              <p class="text-center text-sm-start">
                “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod”
              </p>
              <p class="fw-bold text-center text-sm-start">
                John Dee 32, Bromo
              </p>
            </div>
          </div>
        </div>
      </OwlCarousel>
    </div>
  );
};

export default Carousel;
