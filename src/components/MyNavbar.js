import React from "react";
import { Navbar, Container, Nav, Button } from "react-bootstrap";
import "../style/MyNavbar.css";
import { Link } from "react-router-dom";

const MyNavbar = () => {
  return (
    <div>
      <Navbar className="navbar py-3 fixed-top" expand="lg">
        <Container>
          <Navbar.Brand href="#home" className="fw-bold">
            <Link to="/" className="text-black text-decoration-none">
              BCR
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link href="#home" className="text-black nav-link me-3">
                Our Service
              </Nav.Link>
              <Nav.Link href="#home" className="text-black nav-link me-3">
                Why Us
              </Nav.Link>
              <Nav.Link href="#home" className="text-black nav-link me-3">
                Testimonial
              </Nav.Link>
              <Nav.Link href="#home" className="text-black nav-link me-3">
                FAQ
              </Nav.Link>
            </Nav>
            <Link to="/register">
              <Button className="btn-register text-decoration-none border-0 fw-bold px-3 py-2">
                Register
              </Button>
            </Link>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default MyNavbar;
