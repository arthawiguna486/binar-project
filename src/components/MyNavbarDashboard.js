import React from "react";
import "../style/MyNavbarDashboard.css";
import {
  Navbar,
  Container,
  Form,
  Button,
  Nav,
  NavDropdown,
  InputGroup,
  Col,
} from "react-bootstrap";
import logo from "../asset/logo.png";
import avatar from "../asset/avatar.svg";
import menu from "../asset/fi_menu.svg";
import search from "../asset/fi_search.svg";

const MyNavbarDashboard = () => {
  return (
    <Navbar
      className="navbar-dashboard fixed-top"
      bg="white"
      expand="lg"
      style={{ zIndex: "100" }}
    >
      <Container>
        <Navbar.Brand href="#" className="me-5">
          <img src={logo} alt="" width="100" height="34" />
        </Navbar.Brand>
        <div className="menu ms-5">
          <img src={menu} alt="" width="24" height="24" />
        </div>
        <div className="ms-auto d-flex">
          <Form className="d-flex">
            {/* <img src={search} alt="search" /> */}
            <Form.Group as={Col} md="8" controlId="validationCustomUsername">
              <InputGroup hasValidation>
                <InputGroup.Text id="inputGroupPrepend">
                  <img src={search} alt="search" />
                </InputGroup.Text>
                <Form.Control
                  type="text"
                  placeholder="Search"
                  aria-describedby="inputGroupPrepend"
                  className="border-start-0 rounded-0"
                />
              </InputGroup>
            </Form.Group>
            <Button className="btn-search text-decoration-none bg-white fw-bold px-3">
              Search
            </Button>
          </Form>
          <img src={avatar} alt="" className="ms-4" />
          <Nav className="ms-2">
            <NavDropdown
              title="Unis Badri"
              id="navbarScrollingDropdown"
              className="text-black fs-4"
            >
              <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action4">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action5">
                Something else here
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </div>
      </Container>
    </Navbar>
  );
};

export default MyNavbarDashboard;
