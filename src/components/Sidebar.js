import React from "react";
import truck from "../asset/fi_truck.svg";
import home from "../asset/fi_home.svg";
import "../style/Sidebar.css";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";

const Sidebar = () => {
  const location = useLocation();
  console.log(location);

  return (
    <div
      className="left-bar h-100 d-flex justify-content-start position-fixed"
      style={{ zIndex: "100" }}
    >
      <div className="main-menu top-0 start-0">
        <div className="main-menu-rectangle mt-3 mx-auto mb-2"></div>
        <Link to="/dashboard" className="text-decoration-none">
          <div
            className={
              "home text-center mt-3 " +
              (location.pathname === "/dashboard" ||
              location.pathname === "/dashboard/"
                ? "active-state"
                : "nonActive-state")
            }
          >
            <img
              src={home}
              alt="home"
              width="24"
              height="24"
              className="d-block mx-auto"
            />
            <p className="text-white mt-1 fw-bold">Home</p>
          </div>
        </Link>
        <Link to="/dashboard/listCar" className="text-decoration-none">
          <div
            className={
              "car text-center mt-3 " +
              (location.pathname === "/dashboard/listCar" ||
              location.pathname === "/dashboard/listCar/addNewCar"
                ? "active-state"
                : "nonActive-state")
            }
          >
            <img
              src={truck}
              alt=""
              width="24"
              height="24"
              className="d-block mx-auto"
            />
            <p className="text-white mt-1 fw-bold">Car</p>
          </div>
        </Link>
      </div>
      {location.pathname === "/dashboard" ? (
        <div className="side-bar bg-white">
          <h3>DASHBOARD</h3>
          <p>Dashboard</p>
        </div>
      ) : (
        <div className="side-bar bg-white">
          <h3>CARS</h3>
          <p>List Car</p>
        </div>
      )}
    </div>
  );
};

export default Sidebar;
