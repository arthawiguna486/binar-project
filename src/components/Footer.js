import React from "react";
import instagram from "../asset/icon_instagram.svg";
import facebook from "../asset/icon_facebook.svg";
import twitter from "../asset/icon_twitter.svg";
import twitch from "../asset/icon_twitch.svg";
import logo from "../asset/logo.png";

const Footer = () => {
  return (
    <div class="container">
      <div class="row gx-md-5 footer justify-content-between">
        <div class="col-xl-3 col-sm-5 col-md-7 mb-sm-5 addres">
          <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
          <p>binarcarrental@gmail.com</p>
          <p>081-233-334-808</p>
        </div>
        <div class="col-xl-2 col-sm-5 col-md-4 mb-sm-5 navigation">
          <a href="#our-service" class="text-decoration-none text-dark fw-bold">
            <p>Our Service</p>
          </a>
          <a href="#why-us" class="text-decoration-none text-dark fw-bold">
            <p>Why Us</p>
          </a>
          <a href="#testimony" class="text-decoration-none text-dark fw-bold">
            <p>Testimonial</p>
          </a>
          <a href="#faq" class="text-decoration-none text-dark fw-bold">
            <p>FAQ</p>
          </a>
        </div>
        <div class="col-xl-3 col-sm-5 col-md-7 sosmed">
          <p>Connect With Us</p>
          <div class="icon-sosmed ">
            <a
              href="https://www.facebook.com/"
              class="text-decoration-none image"
            >
              <img src={facebook} alt="icon_facebook" />
            </a>
            <a href="https://www.instagram.com/" class="text-decoration-none">
              <img src={instagram} alt="icon_instagram" />
            </a>
            <a href="https://twitter.com/" class="text-decoration-none">
              <img src={twitter} alt="icon_twitter" />
            </a>
            <a href="https://www.twitch.tv/" class="text-decoration-none">
              <img src={twitch} alt="icon_twitch" />
            </a>
          </div>
        </div>
        <div class="col-xl-2 col-sm-5 col-md-4 copyright">
          <p>Copyright Binar 2022</p>
          <a href="#header">
            <img src={logo} alt="logo" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
