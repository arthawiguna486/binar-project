import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const userRegister = createAsyncThunk(
  "auth/userRegister",
  async (data) => {
    try {
      const response = await axios.post(
        `http://notflixtv.herokuapp.com/api/v1/users`,
        data
      );
      console.log(response, "di register action");
      return response.data.data;
    } catch (error) {
      console.log(error.response);
    }
  }
);

export const userLogin = createAsyncThunk(
  "auth/userLogin",
  async (data, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        `http://notflixtv.herokuapp.com/api/v1/users/login`,
        data
      );
      console.log(response, "di login action");
      return response.data.data;
    } catch (error) {
      console.log(error.response.data);
      return rejectWithValue(error.response.data);
    }
  }
);

export const userAdminLogin = createAsyncThunk(
  "auth/userAdminLogin",
  async (data) => {
    return data;
  }
);

const initialState = {
  user: null,
  status: "idle",
  error: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {},
  extraReducers: {
    [userRegister.pending]: (state) => {
      state.status = "loading";
    },
    [userRegister.fulfilled]: (state, action) => {
      state.status = "succes";
      state.user = action.payload;
      localStorage.setItem(
        "token-register",
        JSON.stringify(action.payload.token)
      );
    },
    [userRegister.rejected]: (state, action) => {
      state.status = "rejected";
      state.error = action.payload.message;
    },
    [userLogin.pending]: (state) => {
      state.status = "loading";
    },
    [userLogin.fulfilled]: (state, action) => {
      state.status = "success";
      state.user = action.payload;
      localStorage.setItem("token-login", JSON.stringify(action.payload.token));
    },
    [userLogin.rejected]: (state, action) => {
      state.status = "rejected";
      state.error = action.payload.message;
    },
    [userAdminLogin.pending]: (state) => {
      state.status = "loading";
    },
    [userAdminLogin.fulfilled]: (state, action) => {
      state.user = action.payload.email;
      console.log(state.user);
    },
    [userAdminLogin.rejected]: (state, action) => {
      state.status = "rejected";
      state.error = action.payload.message;
    },
  },
});

export const getUser = (state) => state.auth.user;
export const getStatus = (state) => state.auth.status;
export const getError = (state) => state.auth.error;
export default authSlice.reducer;
export const { setErrorMessage } = authSlice.actions;
