import React, { useEffect, useState } from "react";
import MyNavbar from "../components/MyNavbar";
import Footer from "../components/Footer";
import { Form, Accordion } from "react-bootstrap";
import "../style/Detail.css";
import car from "../asset/car.png";
import icUser from "../asset/fi_users.svg";
import icSettings from "../asset/fi_settings.svg";
import iccalendar from "../asset/fi_calendar.svg";
import axios from "axios";
import { useParams } from "react-router";

// image preview
import ModalImage from "react-modal-image";
import { Link } from "react-router-dom";

const Detail = () => {
  const { id } = useParams();
  const [detailData, setDetailData] = useState({ car: [] });

  const getDetailCar = async () => {
    const response = await axios.get(
      `https://rent-cars-api.herokuapp.com/admin/car/${id}`
    );
    console.log(response);
    setDetailData({ detailData: response.data });
  };
  console.log(detailData);
  useEffect(() => {
    window.scrollTo(0, 0);
    getDetailCar();
  }, []);

  return (
    <div>
      <MyNavbar />
      <div className="bg-header w-100"></div>
      <div
        className="panel bg-white shadow-sm w-75 mx-auto px-4 pt-3 pb-1 rounded-3"
        style={{ marginTop: "-44px" }}
      >
        <h5 className="fw-bold fs-6">Pencarianmu</h5>
        <Form>
          <div className="d-flex justify-content-between gap-4">
            <Form.Group className="mb-3 w-100" controlId="driver">
              <Form.Label style={{ fontSize: "12px", color: "#8A8A8A" }}>
                Tipe Driver
              </Form.Label>
              <Form.Control
                type="text"
                style={{ backgroundColor: "#D0D0D0" }}
              />
            </Form.Group>
            <Form.Group className="mb-3 w-100" controlId="driver">
              <Form.Label style={{ fontSize: "12px", color: "#8A8A8A" }}>
                Tanggal
              </Form.Label>
              <Form.Control
                type="text"
                style={{ backgroundColor: "#D0D0D0" }}
              />
            </Form.Group>
            <Form.Group className="mb-3 w-100" controlId="driver">
              <Form.Label style={{ fontSize: "12px", color: "#8A8A8A" }}>
                Waktu Jemput/Ambil
              </Form.Label>
              <Form.Control
                type="text"
                style={{ backgroundColor: "#D0D0D0" }}
              />
            </Form.Group>
            <Form.Group className="mb-3 w-100" controlId="driver">
              <Form.Label style={{ fontSize: "12px", color: "#8A8A8A" }}>
                Jumlah Penumpang (optional)
              </Form.Label>
              <Form.Control
                type="text"
                style={{ backgroundColor: "#D0D0D0" }}
              />
            </Form.Group>
          </div>
        </Form>
      </div>
      <div className="w-75 mx-auto d-flex justify-content-between mt-4 px-0 gap-5">
        <div
          className="card-left bg-white mx-auto px-4 pt-3 pb-1 rounded-3"
          style={{ width: "680px" }}
        >
          <h5 className="fw-bold fs-6">Tentang Paket</h5>
          <p>Include</p>
          <ul style={{ fontSize: "14px", color: "#8A8A8A" }}>
            <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
            <li>Sudah termasuk bensin selama 12</li>
            <li>udah termasuk Tiket Wisata</li>
            <li>Sudah termasuk pajak</li>
          </ul>
          <p>Exclude</p>
          <ul style={{ fontSize: "14px", color: "#8A8A8A" }}>
            <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
            <li>
              Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
              20.000/jam
            </li>
            <li>Tidak termasuk akomodasi penginapan</li>
          </ul>
          <Accordion className="border-0">
            <Accordion.Item eventKey="0" className="border-0">
              <Accordion.Header>
                <h5 className="fw-bold fs-6">Refund, Reschedule, Overtime</h5>
              </Accordion.Header>
              <Accordion.Body>
                <ul style={{ fontSize: "14px", color: "#8A8A8A" }}>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                  <li>Tidak termasuk biaya makan sopir Rp 75.000/hari </li>
                </ul>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>
        <div
          className="card-right bg-white mx-auto px-4 pt-5 pb-4 pb-1 rounded-3"
          style={{ width: "405px" }}
        >
          <ModalImage
            className="modal-image d-block mx-auto"
            small={car}
            large={car}
          />
          <div className="pb-2">
            <h5 className="fw-bold fs-6 mt-4">Avanza / Small</h5>
            <div className="d-flex justify-content-start gap-3 mt-2">
              <div className="d-flex gap-2">
                <img src={icUser} alt="" width="12" height="12" />
                <p style={{ fontSize: "10px", color: "#8A8A8A" }}>4 Orang</p>
              </div>
              <div className="d-flex gap-2">
                <img src={icSettings} alt="" width="12" height="12" />
                <p style={{ fontSize: "10px", color: "#8A8A8A" }}>Manual</p>
              </div>
              <div className="d-flex gap-2">
                <img src={iccalendar} alt="" width="12" height="12" />
                <p style={{ fontSize: "10px", color: "#8A8A8A" }}>Tahun 2020</p>
              </div>
            </div>
            <div className="d-flex justify-content-between mt-4">
              <h6>Total</h6>
              <h5 className="fw-bold fs-6">Rp 430.000</h5>
            </div>
            <Link to="/invoice">
              <button
                className="px-4 py-2 fw-bold text-white border-0 w-100 mt-3"
                style={{ backgroundColor: "#5CB85F" }}
              >
                Lanjutkan Pembayaran
              </button>
            </Link>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <Link to="/invoice">
          <button
            className="px-4 py-2 fw-bold text-white border-0  mt-3"
            style={{ backgroundColor: "#5CB85F" }}
          >
            Lanjutkan Pembayaran
          </button>
        </Link>
      </div>
      <Footer />
    </div>
  );
};

export default Detail;
