import React from "react";
import MyNavbar from "../components/MyNavbar";
import Carousel from "../components/Carousel";
import "../style/Home.css";
import imageCar from "../asset/mercedes.png";
import imgService from "../asset/img-service.png";
import checkList from "../asset/check-list.png";
import complete from "../asset/icon_complete.png";
import price from "../asset/icon_price.png";
import hours from "../asset/icon_24hrs.png";
import professional from "../asset/icon_professional.png";
import backgroundBlue from "../asset/bg-hero.png";
import Footer from "../components/Footer";
import { Accordion, Container } from "react-bootstrap";
import { Helmet } from "react-helmet";

// Chart
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";

const Home = () => {
  const title = "Home - Binar Car Rental";

  // Chart Register
  ChartJS.register(ArcElement, Tooltip, Legend);

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
    },
  };

  const data = {
    labels: ["Agya", "Avanza", "Ayla", "Xenia"],
    datasets: [
      {
        label: "# of Votes",
        data: [40, 3, 37, 20],
        backgroundColor: [
          "rgb(146,208,148)",
          "rgb(250,44,90)",
          "rgb(249,204,0)",
          "rgb(13,40,166)",
        ],
        borderColor: [
          "rgb(197,248,199)",
          "rgb(255,167,167)",
          "rgb(250,213,157)",
          "rgb(124,144,239)",
        ],
        borderWidth: 4,
      },
    ],
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <MyNavbar />
      <div className="jumbotron">
        <div className="container container-jumbotron">
          <div className="row">
            <div className="left col-md-6">
              <h1 className="title-jumbotron">
                Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
              </h1>
              <p className="desc-jumbotron">
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas <br /> terbaik dengan harga terjangkau. Selalu siap
                melayani kebutuhanmu <br /> untuk sewa mobil selama 24 jam.
              </p>
            </div>
            <div className="right col-md-6">
              <img className="bg-blue" src={backgroundBlue} alt="bg-blue" />
              <img className="img-car" src={imageCar} alt="mercedes" />
            </div>
          </div>
        </div>
      </div>
      {/* service */}
      <div class="container" id="our-service">
        <div class="row our-service justify-content-around">
          <div class="col-lg-5 col-10 img-service">
            <img src={imgService} class="img-fluid" alt="services" />
          </div>
          <div class="col-lg-5 col-10 desc-service">
            <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
            <p class="main-desc">
              Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
              lebih murah dibandingkan yang lain, kondisi mobil baru, serta
              kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
              wedding, meeting, dll.
            </p>
            <div class="service-list d-flex mt-4 mt-lg-0 justify-content-center justify-content-lg-start">
              <div class="icon-list me-3">
                <img src={checkList} class="d-block" alt="check-list" />
                <img src={checkList} class="d-block mt-3" alt="check-list" />
                <img src={checkList} class="d-block mt-3" alt="check-list" />
                <img src={checkList} class="d-block mt-3" alt="check-list" />
                <img src={checkList} class="d-block mt-3" alt="check-list" />
              </div>
              <div class="desc-list">
                <p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                <p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                <p class="pt-1">Sewa Mobil Jangka Panjang Bulanan</p>
                <p class="pt-1">Gratis Antar - Jemput Mobil di Bandara</p>
                <p class="pt-1">Layanan Airport Transfer / Drop In Out</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* why us */}
      <div class="container" id="why-us">
        <div class="row why-us-header justify-content-xl-start text-xl-start justify-content-center text-center">
          <div class="col-sm-8 why-title">
            <h2>Why Us</h2>
            <p>Mengapa harus pilih Binar Car Rental?</p>
          </div>
          <div class="row why-us-content justify-content-xl-between justify-content-center">
            <div class="col-lg-5 col-sm-10 me-md-4 me-xl-0 mb-md-2 mb-0 why-us-card">
              <img src={complete} alt="icon_complete" />
              <h3>Mobil Lengkap</h3>
              <p>
                Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                terawat
              </p>
            </div>
            <div class="col-lg-5 col-sm-10 me-md-4 me-xl-0 why-us-card">
              <img src={price} alt="icon_price" />
              <h3>Harga Murah</h3>
              <p>
                Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                terawat
              </p>
            </div>
            <div class="col-lg-5 col-sm-10 why-us-card me-md-4 me-xl-0">
              <img src={hours} alt="icon_24hrs" />
              <h3>Layanan 24 Jam</h3>
              <p>
                Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                terawat
              </p>
            </div>
            <div class="col-lg-5 col-sm-10 me-md-4 me-xl-0 why-us-card">
              <img src={professional} alt="icon_professional" />
              <h3>Sopir Profesional</h3>
              <p>
                Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                terawat
              </p>
            </div>
          </div>
        </div>
      </div>

      {/* Chart */}
      <div className="chart w-25 h-25 mx-auto">
        <h2 className="text-center mb-3">Data Sewa Mobil</h2>
        <Pie options={options} data={data} />
      </div>

      {/* testimonial */}
      <Carousel />

      {/* content blue */}
      <div class="container">
        <div class="row content-blue justify-content-center">
          <div class="content-blue-desc col-xl-12 text-center">
            <h1>Sewa Mobil di (Lokasimu) Sekarang</h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
            </p>
            <a href="#header">
              <button class="btn btn-mulai-sewa text-light fw-bold">
                Mulai Sewa Mobil
              </button>
            </a>
          </div>
        </div>
      </div>

      {/* Faq */}
      <Container>
        <div className="faq d-flex justify-content-between">
          <div className="faq-title">
            <h2>Frequently Asked Question</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <Accordion className="faq-content">
            <Accordion.Item eventKey="0" className="accordion mb-3">
              <Accordion.Header className="question">
                <p className="my-auto"> Apa saja syarat yang dibutuhkan?</p>
              </Accordion.Header>
              <Accordion.Body>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="1" className="accordion mb-3">
              <Accordion.Header className="question">
                <p className="my-auto">
                  Berapa hari minimal sewa mobil lepas kunci?
                </p>
              </Accordion.Header>
              <Accordion.Body>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="2" className="accordion mb-3">
              <Accordion.Header className="question">
                <p className="my-auto">
                  Berapa hari sebelumnya sabaiknya booking sewa mobil?
                </p>
              </Accordion.Header>
              <Accordion.Body>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="3" className="accordion mb-3">
              <Accordion.Header className="question">
                <p className="my-auto">Apakah Ada biaya antar-jemput?</p>
              </Accordion.Header>
              <Accordion.Body>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="4" className="accordion mb-3">
              <Accordion.Header className="question">
                <p className="my-auto">Bagaimana jika terjadi kecelakaan?</p>
              </Accordion.Header>
              <Accordion.Body>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>
      </Container>

      {/* Footer */}
      <Footer />
    </div>
  );
};

export default Home;
