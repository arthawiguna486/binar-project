import React, { useEffect, useRef } from "react";
import MyNavbar from "../components/MyNavbar";
import Footer from "../components/Footer";
import "../style/Invoice.css";
import arrowLeft from "../asset/fi_arrow-left.svg";
import success from "../asset/success.svg";
import checkList from "../asset/checklist.svg";
import download from "../asset/fi_download.svg";
import checkThree from "../asset/check-three.svg";
import { gsap } from "gsap";

//pdf
import { Viewer, Worker } from "@react-pdf-viewer/core";
import { getFilePlugin } from "@react-pdf-viewer/get-file";
// Import styles
import "@react-pdf-viewer/core/lib/styles/index.css";
import pdfInvoice from "../asset/pdf-test.pdf";

const Invoice = () => {
  const getFilePluginInstance = getFilePlugin();
  const { Download } = getFilePluginInstance;
  const ref = useRef();

  useEffect(() => {
    window.scrollTo(0, 0);
    gsap.to(ref.current, {
      duration: 2,
      y: 42,
      opacity: 1,
      ease: "bounce",
      rotation: "+=360",
    });
  }, []);

  return (
    <div>
      <MyNavbar />
      <div className="bg-header invoice-header w-100"></div>
      <div className="d-flex" style={{ marginTop: "-64px" }}>
        <div
          className="d-flex mx-auto justify-content-between"
          style={{ width: "1200px" }}
        >
          <div className="d-flex gap-3">
            <img src={arrowLeft} className="" alt="" width="24" height="24" />
            <div>
              <h5 className="fw-bold fs-6">Tiket</h5>
              <p style={{ fontSize: "12px", fontWeight: "300" }}>
                Order ID : xxxxxxxx
              </p>
            </div>
          </div>
          <div className="d-flex gap-3">
            <div className="d-flex gap-2">
              <img src={checkList} className="" alt="" width="12" height="12" />
              <p style={{ fontSize: "12px", fontWeight: "300" }}>
                Pilih Metode
              </p>
            </div>
            <div
              className="mt-2"
              style={{
                backgroundColor: "#0D28A6",
                width: "28px",
                height: "1px",
              }}
            ></div>
            <div className="d-flex gap-2">
              <img src={checkList} className="" alt="" width="12" height="12" />
              <p style={{ fontSize: "12px", fontWeight: "300" }}>
                Pilih Metode
              </p>
            </div>
            <div
              className="mt-2"
              style={{
                backgroundColor: "#0D28A6",
                width: "28px",
                height: "1px",
              }}
            ></div>
            <div className="d-flex gap-2">
              <img
                src={checkThree}
                className=""
                alt=""
                width="12"
                height="12"
              />
              <p style={{ fontSize: "12px", fontWeight: "300" }}>
                Pilih Metode
              </p>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="mt-5">
          <img
            className="d-block mx-auto success"
            src={success}
            alt=""
            width="48"
            height="48"
            ref={ref}
          />
          <h5 className="fw-bold fs-6 text-center mt-5">
            Pembayaran Berhasil!
          </h5>
          <p className="text-center mt-2">
            Tunjukkan invoice ini ke petugas BCR di titik temu.
          </p>
        </div>
        <div
          className="bg-white mx-auto p-5 rounded-3 mt-5"
          style={{
            width: "605px",
            boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.15)",
          }}
        >
          <div className="d-flex justify-content-between">
            <div>
              <h5 className="fw-bold fs-6">Tiket</h5>
              <p style={{ fontSize: "12px", fontWeight: "300" }}>*invoice</p>
            </div>

            <Download>
              {(props) => (
                <button
                  className="download-invoice d-flex px-4 h-25 py-2 border-2 fw-bold rounded-1"
                  onClick={props.onClick}
                >
                  <img
                    className="me-2 align-items-center"
                    src={download}
                    alt=""
                    width="18"
                    height="18"
                  />
                  Unduh
                </button>
              )}
            </Download>
          </div>

          <div
            className="d-flex w-100 border-secondary p-5 justify-content-center mt-3"
            style={{ borderStyle: "dashed", backgroundColor: "#D0D0D0" }}
          >
            <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.min.js">
              <Viewer fileUrl={pdfInvoice} plugins={[getFilePluginInstance]} />
            </Worker>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Invoice;
