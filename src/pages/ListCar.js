import React, { useEffect, useState } from "react";
import "../style/Dashboard.css";
import { Card, Toast, Spinner } from "react-bootstrap";
import { Helmet } from "react-helmet";
import key from "../asset/fi_key.svg";
import clock from "../asset/fi_clock.svg";
import trash from "../asset/fi_trash.svg";
import edit from "../asset/fi_edit.svg";
import chevronRight from "../asset/chevron-right.svg";
import plus from "../asset/fi_plus.svg";
import axios from "axios";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { setShowToast } from "../features/carSlice";
import { useDispatch } from "react-redux";

const ListCar = () => {
  const title = "ListCar - Binar Car Rental";
  const [carData, setCarData] = useState({ car: [] });
  const [loading, setLoading] = useState(true);
  const show = useSelector((state) => state.car.showToast);
  // console.log(show);
  const dispatch = useDispatch();

  const getCar = async () => {
    try {
      const response =
        await axios.get(`https://rent-cars-api.herokuapp.com/admin/car
      `);
      console.log(response);
      setCarData({ car: response.data });
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCar();
  }, []);

  return (
    <div className="content-list-car h-100 px-5 py-4 ">
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Toast
        onClose={() => dispatch(setShowToast(false))}
        show={show}
        delay={3000}
        autohide
        className="px-5 py-1 bg-success text-white position-absolute text-center"
        style={{ left: 710 }}
      >
        <Toast.Body>Data Berhasil Disimpan!</Toast.Body>
      </Toast>

      <div>
        <p>
          <span className="fw-bold">Cars</span>{" "}
          <img src={chevronRight} alt="right" /> List Cars
        </p>
        <div className="d-flex justify-content-between">
          <h2 className="list-car">List Car</h2>
          <Link to="/dashboard/listCar/addNewCar">
            <button className="btn-add-new-car px-4 border-0 fw-bold">
              <img src={plus} alt="plus" className="me-3" /> Add New Car
            </button>
          </Link>
        </div>
        <div className="btn-category d-flex gap-2 mt-3">
          <button className="active px-3 fw-bold border-1 solid rounded-1">
            All
          </button>
          <button className="non-active px-3 fw-bold border-1 solid rounded-1">
            Small
          </button>
          <button className="non-active px-3 fw-bold border-1 solid rounded-1">
            Medium
          </button>
          <button className="non-active px-3 fw-bold border-1 solid rounded-1">
            Large
          </button>
        </div>
        {/* card */}
        <div className="card-list row mt-4">
          {loading ? (
            <div
              className="d-flex justify-content-center"
              style={{
                height: "100vh",
                marginLeft: "310px",
                marginTop: "100px",
              }}
            >
              <Spinner animation="border" role="status">
                <span className="visually-hidden w-75 h-75">Loading...</span>
              </Spinner>
            </div>
          ) : (
            carData.car.map((i) => (
              <div className="col-md-4 mb-3">
                <Card className="w-100">
                  <Link to={{ pathname: `/detail/${i.id}` }}>
                    <img
                      src={i.image}
                      alt="car"
                      width="270"
                      height="160"
                      className="img-car d-block mx-auto"
                    />
                  </Link>
                  <Card.Body className="px-4 pb-4">
                    <Card.Text style={{ fontSize: "14px", fontWeight: 400 }}>
                      <span>{i.name}</span> / <span>{i.category}</span>
                    </Card.Text>
                    <Card.Title style={{ fontSize: "16px", fontWeight: 700 }}>
                      Rp.{i.price}/Hari
                    </Card.Title>
                    <div className="mt-3">
                      <div className="d-flex gap-2">
                        <img
                          src={key}
                          alt=""
                          width="20"
                          height="20"
                          className=""
                        />
                        {i.start_rent_at && i.finish_rent_at !== null ? (
                          <p>
                            {i.start_rent_at} - {i.finish_rent_at}
                          </p>
                        ) : (
                          <p>Start rent - Finish rent</p>
                        )}
                      </div>
                      <div className="d-flex gap-2">
                        <img
                          src={clock}
                          alt=""
                          width="20"
                          height="20"
                          className=""
                        />
                        <p>{i.updatedAt}</p>
                      </div>
                    </div>
                    <div className="d-flex justify-content-between gap-4 mt-3">
                      <button className="btn-delete px-4 d-flex align-items-center justify-content-center fw-bold w-100 py-3 bg-white">
                        <img
                          src={trash}
                          alt=""
                          width="18"
                          height="18"
                          className=""
                        />
                        Delete
                      </button>
                      <button className="btn-edit px-4 d-flex align-items-center justify-content-center border-0 text-white fw-bold w-100 py-3">
                        <img
                          src={edit}
                          alt=""
                          width="18"
                          height="18"
                          className=""
                        />
                        Edit
                      </button>
                    </div>
                  </Card.Body>
                </Card>
              </div>
            ))
          )}
        </div>
      </div>
    </div>
  );
};

export default ListCar;
