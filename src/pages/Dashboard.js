import React from "react";
import MyNavbarDashboard from "../components/MyNavbarDashboard";
import Sidebar from "../components/Sidebar";
import "../style/Dashboard.css";
import { Outlet } from "react-router";

const Dashboard = () => {
  return (
    <div className="dashboard w-100">
      <MyNavbarDashboard />
      <div className="d-flex justify-content-start w-100 gap-0 position-relative">
        <Sidebar />
        <Outlet />
      </div>
    </div>
  );
};

export default Dashboard;
